print("------ LIST -------\n")

x = [1, 2, 3, 4, 5, 6, 7 ,8] # List

for i in x:
    print(i)

print("--------------------------------\n")


print("------ TUPLE -------\n")

z = (1, 2, 3, 4, 5, 6) # Tuple


for i in z:
    print(i)

print("--------------------------------\n")

print("------ Range -------\n")

w = range(5) #Range

for i in w:
    print(i)

print("--------------------------------\n")

print("------ Range from - to -------\n")

y = range(4, 10) #range from to

for i in y:
    print(i)
print("--------------------------------\n")

print("------ Range from - to - step -------\n")

p = range(2, 10, 2) #range from to, step

for i in p:
    print(i)

print("--------------------------------\n")


print("------ Dictionary -------\n")

d = {
    'firstName'   : 'Juan',
    'lastName' : 'Saldarriaga',
    'age'      : 43,
    'address' : 'Calle 59 # 23 - 32'
    }

for index, value in d.items():
    print(value)    