# and       y
# or        ó
# not       NO
# in        valor en el conjunto
# not in    No existe en el conjunto
# is        identidad del mismo objeto
# is not    la identidad no es del mismo objeto

a = True 
b = False

x = ('perro', 'gato', 'Gallina', 'Ballena', 'Elefante')

y = 'gato'

if  a is not b:
    print('La expresión es verdadera')
else:
    print('La expresión es falsa')    