# Operadores de comparación

# <     Menor qué
# >     Mayor qué
# <=    Menor o igual qué
# >=    Mayor o igual qué
# ==    Igual a
# !=    Diferente dé

x = 60
y = 24

if x != y:
    print('Verdad')
else:
    print('Falso')    