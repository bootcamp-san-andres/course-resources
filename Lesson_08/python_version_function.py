import platform

def main():
    print('Main function')
    message()

def message():
    print('message function')
    print('This is Python Version {} '.format(platform.python_version()))

if __name__ == "__main__":
    main()        