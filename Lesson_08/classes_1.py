class Automoviles:
    color  = 'Blanco'
    placa  = 'AFC 87F'
    serial = 2344555

    def set_color(self, color):
        self.color = color

    def get_color(self):
        return self.color

    def set_placa(self, placa):
        self.placa = placa
    def get_placa(self):
        return self.placa    

def main():
    auto = Automoviles()
    auto.set_color('Rojo')
    print(auto.get_color())
    print(auto.get_placa())

if __name__ == "__main__":
    main()             
