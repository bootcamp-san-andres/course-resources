class Saludos:
    
    def saludo_maniana(self, nombre):
        print('Buenos Días {}'.format(nombre))

    def saludo_tarde(self):
        print('Buenas tardes')

    def saludo_noche(self):
        print('Buenas noches')

    def despedida(self):
        print('Hasta luego!')

def main():
    my_saludo = Saludos()
    my_saludo.saludo_maniana("Carlos")
  

if __name__ == "__main__":
    main()                    