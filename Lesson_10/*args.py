# *args son argumentos que pasan como una tupla, 
# El paso de estos argumentos desde una variable debe 
# realizarse agregando un * antes de la variable
# Ejemplo
# x = (1, 3, 4)
# calcular(*x) # aquí se agrega el * para indicar que son multiples parametros en formato tupla

def main():
    people('John', 'Jaime', 'María', 'Sandra') # Argumentos pasan como una tupla en  *args

def people(*args):
    if len(args):
        for name in args:
            print(name)
    else:
        print('NoBody..') 

if __name__ == "__main__":
    main()