# **kwargs son parametros que pasan como un diccionario clave valor, 
# se debe indicar la variable con dos ** para pasarla como parametro kwargs 

def main():
    values = {'firstName': 'John', 'lastName' :'Gongora', 'age' : 34, 'address' : 'Callse 10 - 23'}
    people(**values) # Argumentos pasan como una tupla en  *args

def people(**kwargs):
    if len(kwargs):
        for key in kwargs:
            print(f'Clave: {key} Valor: {kwargs[key]}')
    else:
        print('NoBody..') 

if __name__ == "__main__":
    main()